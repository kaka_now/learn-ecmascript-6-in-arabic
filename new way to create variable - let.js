/*
    let, const

    var
    - function scope
    - can be redclare

    let
    - Block Scope
    - cannot be redeclare

    const
    - block scope
    - cannot be redeclare

*/

function varTest() {
    var x = 1;
    if (true) {
        var x = 2;
        console.log(x);
    }
    console.log(x);
    return "Done";
}

function letTest() {
    let x = 1;
    if (true) {
        let x = 2;
        console.log(x);
    }
    console.log(x);
    return "Done";
}

// can be redeclare
var x = 1;
var x = 2;
console.log(x);

// cannot be redeclare
let b = 1;
let b = 2;

const c = 1;
// const c = 2;
